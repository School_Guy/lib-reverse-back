package de.schoolguy.libreverseback.coordconverters;

import de.schoolguy.libreverseback.coordinates.DecimalDegreeCoordinate;
import de.schoolguy.libreverseback.coordinates.DecimalDegreeMinutesCoordinate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DecimalDegreeConverterTest {

    @Test
    void decimalDegreeToDegreeDecimalMinutes() {
        // Arrange
        DecimalDegreeCoordinate coordinate = new DecimalDegreeCoordinate(49.38473,7.75645);
        DecimalDegreeConverter converter = new DecimalDegreeConverter();

        // Act
        DecimalDegreeMinutesCoordinate result = converter.decimalDegreeToDegreeDecimalMinutes(coordinate);

        // Assert
        assertEquals("N 49 23.084 E 007 45.387", result.prettyToString());
    }
}