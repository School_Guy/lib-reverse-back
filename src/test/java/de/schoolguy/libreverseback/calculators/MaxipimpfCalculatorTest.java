package de.schoolguy.libreverseback.calculators;

import de.schoolguy.libreverseback.coordinates.DecimalDegreeCoordinate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxipimpfCalculatorTest {

    @Test
    void calculate() {
        // Arrange
        MaxipimpfCalculator calculator = new MaxipimpfCalculator(892135, 224631, 575288);

        // Act
        DecimalDegreeCoordinate result = calculator.calculate();

        // Assert
        DecimalDegreeCoordinate expected = new DecimalDegreeCoordinate(49.38473, 7.75645);
        assertEquals(expected, result);
    }
}