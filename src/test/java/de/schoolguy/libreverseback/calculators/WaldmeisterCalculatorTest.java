package de.schoolguy.libreverseback.calculators;

import de.schoolguy.libreverseback.coordinates.DecimalDegreeCoordinate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WaldmeisterCalculatorTest {

    @Test
    void calculate() {
        // Arrange
        WaldmeisterCalculator calculator = new WaldmeisterCalculator(0, 0, 0);

        // Act
        DecimalDegreeCoordinate result = calculator.calculate();

        // Assert
        DecimalDegreeCoordinate expected = new DecimalDegreeCoordinate(0.0, 0.0);
        assertEquals(expected, result);
    }
}
