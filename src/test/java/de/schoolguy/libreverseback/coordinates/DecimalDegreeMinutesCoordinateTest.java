package de.schoolguy.libreverseback.coordinates;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DecimalDegreeMinutesCoordinateTest {

    @Test
    void prettyToString() {
        // Arrange
        DecimalDegreeMinutesCoordinate coordinate =
                new DecimalDegreeMinutesCoordinate(11,15.010,49,15.010);

        // Act
        String result = coordinate.prettyToString();

        // Assert
        String expected = "N 49 15.010 E 011 15.010";
        assertEquals(expected, result);
    }

    @Test
    void getLatitude() {
        // Arrange
        DecimalDegreeMinutesCoordinate coordinate =
                new DecimalDegreeMinutesCoordinate(11,15.010,49,15.010);

        // Act
        String result = coordinate.getLatitude();

        // Assert
        String expected = "E 011 15.010";
        assertEquals(expected, result);
    }

    @Test
    void getLongitude() {
        // Arrange
        DecimalDegreeMinutesCoordinate coordinate =
                new DecimalDegreeMinutesCoordinate(11,15.010,49,15.010);

        // Act
        String result = coordinate.getLongitude();

        // Assert
        String expected = "N 49 15.010";
        assertEquals(expected, result);
    }
}