package de.schoolguy.libreverseback.coordinates;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DecimalDegreeCoordinateTest {

    @Test
    void prettyToString() {
        // Arrange
        DecimalDegreeCoordinate coordinate = new DecimalDegreeCoordinate(0.0, 0.0);

        // Act
        String result = coordinate.prettyToString();

        //Assert
        assertEquals("N 0.0 E 0.0", result);
    }

    @Test
    void getLatitude() {
        // Arrange
        DecimalDegreeCoordinate coordinate = new DecimalDegreeCoordinate(0.0, 0.0);

        // Act
        Double result = coordinate.getLatitude();

        //Assert
        assertEquals(0.0, result);
    }

    @Test
    void getLongitude() {
        // Arrange
        DecimalDegreeCoordinate coordinate = new DecimalDegreeCoordinate(0.0, 0.0);

        // Act
        Double result = coordinate.getLongitude();

        //Assert
        assertEquals(0.0, result);
    }
}