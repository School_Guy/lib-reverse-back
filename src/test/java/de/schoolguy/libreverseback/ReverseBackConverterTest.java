package de.schoolguy.libreverseback;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ReverseBackConverterTest {

    @Test
    void getFormattedCoords() {
        // Arrange
        ReverseBackConverter magicBox = new ReverseBackConverter(
                "405119", "820330", "160064", ReverseType.WALDMEISTER);
        magicBox.calculate();

        // Act
        String result = magicBox.getFormattedCoords(CoordinateSystems.DECIMALDEGREEMINUTES);

        //Assert
        assertEquals("N 53 12.281 E 014 22.896", result);
    }

    @Test
    void getSplittedFormattedCoords() {
        // Arrange
        ReverseBackConverter magicBox = new ReverseBackConverter(
                "405119", "820330", "160064", ReverseType.WALDMEISTER);
        magicBox.calculate();

        // Act
        String[] result = magicBox.getSplittedFormattedCoords(CoordinateSystems.DECIMALDEGREEMINUTES);

        // Assert
        String[] expected = {"N 53 12.281", "E 014 22.896"};
        assertArrayEquals(expected, result);
    }
}
