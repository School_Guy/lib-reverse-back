package de.schoolguy.libreverseback;

import de.schoolguy.libreverseback.coordinates.DecimalDegreeCoordinate;

/**
 * This interface contains the contract for calculating Reverse-Wherigos back to coordinates, only knowing the numbers.
 */
public interface CoordinatesCalculator {
    /**
     * This method performs the actual conversion or calls the method which does it. All values needed for the
     * calculation should be handed over in the constructor of the class.
     *
     * @return A coordinate object which contains the final coordinates of the Wherigo.
     */
    DecimalDegreeCoordinate calculate();
}
