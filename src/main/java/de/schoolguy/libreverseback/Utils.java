package de.schoolguy.libreverseback;

/**
 * Class to collect helper methods which ease the porting from Lua to Java code.
 */
public class Utils {
    /**
     * The standard java functions and the Apache Common Library both have no substring-function which offers a method
     * for getting a substring where you have including start and end index.
     *
     * @param str The string to which will be manipulated.
     * @param beginIndex The start index. (Including)
     * @param endIndex The end index. (Including)
     * @return The string from start index to end index.
     */
    public static String inclusiveSubstring(String str, int beginIndex, int endIndex) {
        if (str.length() == endIndex) {
            return str.substring(beginIndex);
        } else {
            return str.substring(beginIndex, endIndex + 1);
        }
    }
}
