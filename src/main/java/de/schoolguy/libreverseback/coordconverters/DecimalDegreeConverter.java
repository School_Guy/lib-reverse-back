package de.schoolguy.libreverseback.coordconverters;

import de.schoolguy.libreverseback.coordinates.DecimalDegreeCoordinate;
import de.schoolguy.libreverseback.coordinates.DecimalDegreeMinutesCoordinate;

/**
 * Converts a Coordinate from Decimal Degree to other formats.
 */
public class DecimalDegreeConverter {

    /**
     * Default empty constructor
     */
    public DecimalDegreeConverter() {
    }

    /**
     * Converts an the coordinate in the parameter to the Format Decimal Degree Minutes.
     *
     * @param coords The coordinate you want to convert.
     * @return The converted coordinate.
     */
    public DecimalDegreeMinutesCoordinate decimalDegreeToDegreeDecimalMinutes(DecimalDegreeCoordinate coords) {
        Integer northCoordInt = coords.getLongitude().intValue();
        Integer eastCoordInt = coords.getLatitude().intValue();
        double northCoord = Math.abs((coords.getLongitude() - northCoordInt) * 60);
        double eastCoord = Math.abs((coords.getLatitude() - eastCoordInt) * 60);
        return new DecimalDegreeMinutesCoordinate(northCoordInt, round(northCoord), eastCoordInt, round(eastCoord));
    }

    /**
     * Helper round method. Values .5 and up will be rounded towards positive infinity and everything else will be
     * rounded towards negative infinity.
     *
     * @param temp The double to round.
     * @return The double which is now rounded.
     */
    private double round(double temp) {
        temp = temp * 1000;
        temp = Math.round(temp);
        temp = temp / 1000;
        return temp;
    }
}

