package de.schoolguy.libreverseback;

/**
 * This interface describes what methods should be available to work with a Coordinate.
 *
 * @param <T> This is the type which is used for passing the information about a certain degree.
 */
public interface Coordinate<T> {
    /**
     * This prints a coordinate in a user readable format.
     *
     * @return The string with the coordinate.
     */
    String prettyToString();

    /**
     * This returns the coordinate-fragment in the given type.
     *
     * @return The Latitude of the coordinate.
     */
    T getLatitude();

    /**
     * This returns the coordinate-fragment in the given type.
     *
     * @return The Longitude of the coordinate.
     */
    T getLongitude();
}
