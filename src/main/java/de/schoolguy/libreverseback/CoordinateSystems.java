package de.schoolguy.libreverseback;

/**
 * All reasonable coordinate systems in which you may want to read the coordinates.
 */
public enum CoordinateSystems {
    DECIMALDEGREE,
    DECIMALDEGREEMINUTES,
    DECIMALDEGREEMINUTESSECONDS,
    UTM
}
