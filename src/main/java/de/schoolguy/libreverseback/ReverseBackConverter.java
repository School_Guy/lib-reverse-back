package de.schoolguy.libreverseback;

import de.schoolguy.libreverseback.coordconverters.DecimalDegreeConverter;
import de.schoolguy.libreverseback.coordinates.DecimalDegreeCoordinate;
import de.schoolguy.libreverseback.coordinates.DecimalDegreeMinutesCoordinate;

/**
 * This is the main interface for a user of this library. With an instance of this class you can solve exactly one
 * Reverse Wherigo.
 */
public class ReverseBackConverter {
    private String firstLine;
    private String secondLine;
    private String thirdLine;
    private ReverseType type;
    private boolean ready;
    private DecimalDegreeCoordinate coordinate;
    private DecimalDegreeConverter converter;

    /**
     * Main constructor of the library. Most applications want to use this and then call calculate. After calculation is
     * one most of the applications want to get the coordinate via one of the other two methods in this class.
     *
     * @param firstLine A 6 digit number which contains the first line.
     * @param secondLine A 6 digit number which contains the second line.
     * @param thirdLine A 6 digit number which contains the third line.
     * @param type One of @see ReverseType
     */
    public ReverseBackConverter(String firstLine, String secondLine, String thirdLine, ReverseType type) {
        this.firstLine = firstLine;
        this.secondLine = secondLine;
        this.thirdLine = thirdLine;
        this.type = type;
        this.coordinate = null;
        this.ready = false;
        this.converter = new DecimalDegreeConverter();
    }

    /**
     * This runs the calculation if a correct type is supplied.
     *
     * @throws IllegalArgumentException If you supply an unknown Reverse-Wherigo-Type.
     */
    public void calculate() {
        CoordinatesCalculator calculator;
        switch (type) {
            case WALDMEISTER:
                // FIXME: Conversation form String to double
                // TODO: Sanity Check
                calculator = new de.schoolguy.libreverseback.calculators.WaldmeisterCalculator(
                        Double.parseDouble(firstLine), Double.parseDouble(secondLine), Double.parseDouble(thirdLine));
                break;
            case MAXIPIMPF:
                calculator = new de.schoolguy.libreverseback.calculators.MaxipimpfCalculator(
                        Integer.parseInt(firstLine), Integer.parseInt(secondLine), Integer.parseInt(thirdLine));
                break;
            default:
                throw new IllegalArgumentException("Unknown Reverse Type selected!");
        }
        coordinate = calculator.calculate();
        ready = true;
    }

    /**
     * This gives you separate elements for each information of the pair.
     *
     * @param coordinateSystem The desired format of the output.
     * @return An array with two elements, where the index 0 holds the longitude and 1 the latitude.
     */
    public String[] getSplittedFormattedCoords(CoordinateSystems coordinateSystem) {
        if (!ready) {
            return new String[]{"", ""};
        }
        switch (coordinateSystem) {
            case DECIMALDEGREE:
                return new String[]{
                        String.valueOf(coordinate.getLongitude()),
                        String.valueOf(coordinate.getLatitude())};
            case DECIMALDEGREEMINUTES:
                DecimalDegreeMinutesCoordinate tmp = converter.decimalDegreeToDegreeDecimalMinutes(coordinate);
                return new String[]{
                        tmp.getLongitude(),
                        tmp.getLatitude()};
            case DECIMALDEGREEMINUTESSECONDS:
            case UTM:
                throw new UnsupportedOperationException("Not implemented yet!");
            default:
                throw new IllegalArgumentException("Unknown Coordinate System selected!");
        }
    }

    /**
     * This gives you a joint information for the coordinate pair.
     *
     * @param coordinateSystem The desired format of the output.
     * @return A string with the result in the format handed over in the parameter of the function.
     * @throws UnsupportedOperationException If the coordinate system is not implemented yet.
     * @throws  IllegalArgumentException If an unknown coordinate system is selected.
     */
    public String getFormattedCoords(CoordinateSystems coordinateSystem) {
        if (!ready) {
            return "";
        }
        switch (coordinateSystem) {
            case DECIMALDEGREE:
                return coordinate.prettyToString();
            case DECIMALDEGREEMINUTES:
                DecimalDegreeMinutesCoordinate tmp = converter.decimalDegreeToDegreeDecimalMinutes(coordinate);
                return tmp.prettyToString();
            case DECIMALDEGREEMINUTESSECONDS:
            case UTM:
                throw new UnsupportedOperationException("Not implemented yet!");
            default:
                throw new IllegalArgumentException("Unknown Coordinate System selected!");
        }
    }
}
