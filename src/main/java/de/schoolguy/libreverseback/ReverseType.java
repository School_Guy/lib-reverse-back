package de.schoolguy.libreverseback;

/**
 * This enum contains all available types of Reverse Wherigos which can be solved with the corresponding codes.
 */
public enum ReverseType {
    WALDMEISTER,
    MAXIPIMPF
}
