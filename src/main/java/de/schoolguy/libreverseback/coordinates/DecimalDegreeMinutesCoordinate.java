package de.schoolguy.libreverseback.coordinates;

import de.schoolguy.libreverseback.Coordinate;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * This class represents a coordinate with the following format: N xx xx.xxx E yy yy.yyy
 */
public class DecimalDegreeMinutesCoordinate implements Coordinate<String> {
    private int latitude;
    private int longitude;
    private double latitudeMinutes;
    private double longitutdeMinutes;
    private boolean north;
    private boolean east;

    private DecimalFormat formatterMinutes;
    private DecimalFormat formatterEastDegrees;

    /**
     * Default constructor to create a coordinate object.
     *
     * @param latitude The latitude of the coordinates
     * @param latitudeMinutes The latitude-minutes of the coordinates
     * @param longitude The longitude of the coordinates
     * @param longitutdeMinutes The longitude-minutes of the coordinates
     */
    public DecimalDegreeMinutesCoordinate(int latitude, double latitudeMinutes, int longitude, double longitutdeMinutes) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.latitudeMinutes = latitudeMinutes;
        this.longitutdeMinutes = longitutdeMinutes;
        north = latitude >= 0;
        east = longitude >= 0;

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');

        formatterMinutes = new DecimalFormat("00.000", otherSymbols);
        formatterEastDegrees = new DecimalFormat("000");
    }

    /**
     * This returns the string representation of this object.
     *
     * @return A string which is unique for every coordinate NOT every object!
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " " + this.prettyToString();
    }

    /**
     * Test
     *
     * @param o Object to compare.
     * @return If the object is an instance of this class and the longitude and latitude match then it returns true,
     * otherwise this returns false.
     */
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof DecimalDegreeMinutesCoordinate)) {
            return false;
        }

        DecimalDegreeMinutesCoordinate coordinate = (DecimalDegreeMinutesCoordinate) o;

        return coordinate.latitude == this.latitude
                && coordinate.longitude == this.longitude
                && coordinate.latitudeMinutes == this.latitudeMinutes
                && coordinate.longitutdeMinutes == this.longitutdeMinutes;
    }

    /**
     * This returns a well formatted String with the coordinate of the object.
     *
     * @return The pretty printed coordinate as a string.
     */
    public String prettyToString() {
        return getLongitude() + " " + getLatitude();
    }

    /**
     * Returns the latitude of the coordinate.
     *
     * @return The latitude as a String.
     */
    public String getLatitude() {
        return (east ? "E " : "W ") + formatterEastDegrees.format(latitude) + " " + formatterMinutes.format(latitudeMinutes);
    }

    /**
     * Return the longitude of the coordinate.
     *
     * @return The longitude as a String.
     */
    public String getLongitude() {
        return (north ? "N " : "S ") + longitude + " " + formatterMinutes.format(longitutdeMinutes);
    }
}
