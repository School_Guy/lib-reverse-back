package de.schoolguy.libreverseback.coordinates;

import de.schoolguy.libreverseback.Coordinate;

/**
 * This class represents a coordinate with the following format: N xx.xxxxxx E yy.yyyyyy
 */
public class DecimalDegreeCoordinate implements Coordinate<Double> {
    private Double latitude;
    private Double longitude;

    /**
     * Default constructor to create a coordinate object.
     *
     * @param latitude The latitude of the coordinate.
     * @param longitude The longitude of the coordinate.
     */
    public DecimalDegreeCoordinate(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * This returns the string representation of this object.
     *
     * @return A string which is unique for every coordinate NOT every object!
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " " + this.prettyToString();
    }

    /**
     * Test
     *
     * @param o Object to compare.
     * @return If the object is an instance of this class and the longitude and latitude match then it returns true,
     * otherwise this returns false.
     */
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof DecimalDegreeCoordinate)) {
            return false;
        }

        DecimalDegreeCoordinate coordinate = (DecimalDegreeCoordinate) o;

        return coordinate.latitude.equals(this.latitude)
                && coordinate.longitude.equals(this.longitude);
    }

    /**
     * This returns a well formatted String with the coordinate of the object.
     *
     * @return The pretty printed coordinate as a string.
     */
    public String prettyToString() {
        return "N " + this.latitude + " E " + this.longitude;
    }

    /**
     * Returns the latitude of the coordinate.
     *
     * @return The latitude as a double.
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Return the longitude of the coordinate.
     *
     * @return The longitude as a double.
     */
    public Double getLongitude() {
        return longitude;
    }
}
