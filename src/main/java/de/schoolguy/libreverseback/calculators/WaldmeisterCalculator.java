package de.schoolguy.libreverseback.calculators;

import de.schoolguy.libreverseback.coordinates.DecimalDegreeCoordinate;
import de.schoolguy.libreverseback.CoordinatesCalculator;

/**
 * This solves Reverse-Wherigos which are created using the following cartridge:
 * http://www.wherigo.com/cartridge/details.aspx?CGUID=dcdcd2ff-c171-4487-93bc-678f6d03ac4f
 */
public class WaldmeisterCalculator implements CoordinatesCalculator {

    private double lineOne;
    private double lineTwo;
    private double lineThree;

    /**
     * Default constructor which takes the 3 x 6 digits as an argument.
     *
     * @param lineOne The first line which is constructed as a number with six digits.
     * @param lineTwo The second line which is constructed as a number with six digits.
     * @param lineThree The third line which is constructed as a number with six digits.
     */
    public WaldmeisterCalculator(double lineOne, double lineTwo, double lineThree) {
        this.lineOne = lineOne;
        this.lineTwo = lineTwo;
        this.lineThree = lineThree;
    }

    /**
     * NOTE: This contains the code which is converted from lua to Java.
     *
     * {@inheritDoc}
     */
    public DecimalDegreeCoordinate calculate() {
        double LongVORZ = 0, LatKOMP = 0, LatVORZ = 0, LongKOMP = 0;

        if (((lineOne % 1000 - lineOne % 100) / 100) == 1) {
            LatVORZ = 1;
            LongVORZ = 1;
        }
        else if (((lineOne % 1000 - lineOne % 100) / 100) == 2) {
            LatVORZ = -1;
            LongVORZ = 1;
        }
        else if (((lineOne % 1000 - lineOne % 100) / 100) == 3) {
            LatVORZ = 1;
            LongVORZ = -1;
        }
        else if (((lineOne % 1000 - lineOne % 100) / 100) == 4) {
            LatVORZ = -1;
            LongVORZ = -1;
        }

        if (((lineThree % 100000 - lineThree % 10000) / 10000 + (lineThree % 100 - lineThree % 10) / 10) % 2 == 0) {
            LongKOMP = LongVORZ * ((lineOne % 100000 - lineOne % 10000) / 10000 * 100
                    + (lineThree % 1000000 - lineThree % 100000) / 100000 * 10 + lineThree % 10
                    + (lineTwo % 1000 - lineTwo % 100) / 100 * 0.1 + (lineTwo % 1000000 - lineTwo % 100000) / 100000
                    * 0.01 + (lineOne % 100 - lineOne % 10) / 10 * 0.001 + (lineThree % 100000 - lineThree % 10000)
                    / 10000 * 1.0E-4 + lineTwo % 10 * 1.0E-5);
        } else if (((lineThree % 100000 - lineThree % 10000) / 10000
                + (lineThree % 100 - lineThree % 10) / 10) % 2 != 0) {
            LongKOMP = LongVORZ * ((lineTwo % 100 - lineTwo % 10) / 10 * 100 + lineThree % 10 * 10
                    + (lineOne % 100 - lineOne % 10) / 10 + (lineOne % 100000 - lineOne % 10000) / 10000 * 0.1
                    + (lineTwo % 1000 - lineTwo % 100) / 100 * 0.01 + lineTwo % 10 * 0.001
                    + (lineThree % 100000 - lineThree % 10000) / 10000 * 1.0E-4
                    + (lineTwo % 100000 - lineTwo % 10000) / 10000 * 1.0E-5);
        }

        if (((lineThree % 100000 - lineThree % 10000) / 10000 + (lineThree % 100 - lineThree % 10) / 10) % 2 == 0) {
            LatKOMP = LatVORZ * ((lineOne % 10000 - lineOne % 1000) / 1000 * 10 + (lineTwo % 100 - lineTwo % 10) / 10
                    + (lineTwo % 100000 - lineTwo % 10000) / 10000 * 0.1 + (lineThree % 1000 - lineThree % 100)
                    / 100 * 0.01 + (lineOne % 1000000 - lineOne % 100000) / 100000 * 0.001
                    + (lineThree % 100 - lineThree % 10) / 10 * 1.0E-4 + lineOne % 10 * 1.0E-5);
        } else if (((lineThree % 100000 - lineThree % 10000) / 10000
                + (lineThree % 100 - lineThree % 10) / 10) % 2 != 0) {
            LatKOMP = LatVORZ * ((lineTwo % 1000000 - lineTwo % 100000) / 100000 * 10 + lineOne % 10
                    + (lineOne % 10000 - lineOne % 1000) / 1000 * 0.1 + (lineThree % 1000000 - lineThree % 100000)
                    / 100000 * 0.01 + (lineThree % 1000 - lineThree % 100) / 100 * 0.001
                    + (lineThree % 100 - lineThree % 10) / 10 * 1.0E-4 + (lineOne % 1000000 - lineOne % 100000)
                    / 100000 * 1.0E-5);
        }
        return new DecimalDegreeCoordinate(LatKOMP, LongKOMP);
    }
}
