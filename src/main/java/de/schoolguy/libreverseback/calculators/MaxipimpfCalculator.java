package de.schoolguy.libreverseback.calculators;

import de.schoolguy.libreverseback.CoordinatesCalculator;
import de.schoolguy.libreverseback.coordinates.DecimalDegreeCoordinate;

import static de.schoolguy.libreverseback.Utils.inclusiveSubstring;

/**
 * This solves Reverse-Wherigos which are created using the following cartridge:
 * http://www.wherigo.com/cartridge/details.aspx?CGUID=e1ddce19-bcad-47c1-bc8c-0349a53f90dc
 */
public class MaxipimpfCalculator implements CoordinatesCalculator {

    private int var_Value1;
    private int var_Value2;
    private int var_Value3;

    /**
     * Default constructor which takes the 3 x 6 digits as an argument.
     *
     * @param zahl1 The first line which is constructed as a number with six digits.
     * @param zahl2 The second line which is constructed as a number with six digits.
     * @param zahl3 The third line which is constructed as a number with six digits.
     */
    public MaxipimpfCalculator(int zahl1, int zahl2, int zahl3) {
        var_Value1 = zahl1;
        var_Value2 = zahl2;
        var_Value3 = zahl3;
    }

    /**
     * {@inheritDoc}
     */
    public DecimalDegreeCoordinate calculate() {
        double[] tmpcoords = Values2LatLong(var_Value1, var_Value2, var_Value3);
        return new DecimalDegreeCoordinate(tmpcoords[0], tmpcoords[1]);
    }

    /**
     * This is the code which is converted from the lua code in the cartridge to Java-Code.
     *
     * @param par_Value1 The first line which is constructed as a number with six digits.
     * @param par_Value2 The second line which is constructed as a number with six digits.
     * @param par_Value3 The third line which is constructed as a number with six digits.
     * @return This is an array with two elements. The index 0 holds the longitude and index 1 holds the latitude.
     */
    private double[] Values2LatLong(int par_Value1, int par_Value2, int par_Value3) {
        int var_Index = 1;
        double var_Lat, var_Long;
        String var_LatLong = " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        int var_Loop = 0;
        int var_Mod, var_NSEW, var_P, var_Step;
        int var_StartPos = 16;
        String var_Value = String.format("%06d", par_Value1) + String.format("%06d", par_Value2) + String.format("%06d", par_Value3);

        var_Mod = Integer.parseInt(inclusiveSubstring(var_Value, 0, 0));
        var_Step = Math.max(1, Integer.parseInt(inclusiveSubstring(var_Value, 1, 1)));
        var_Value = var_Value.substring(2);
        while (var_Loop <= var_Step - 1) {
            var_P = var_StartPos - var_Loop;
            while (var_P >= 1) {
                String var_LatLong_1 = inclusiveSubstring(var_LatLong, 0, var_P - 2);
                String var_LatLong_2 = String.valueOf((Integer.parseInt(inclusiveSubstring(var_Value, var_Index - 1, var_Index - 1)) - var_Mod + 10) % 10);
                String var_LatLong_3 = inclusiveSubstring(var_LatLong, var_P, var_LatLong.length());
                var_LatLong = var_LatLong_1 + var_LatLong_2 + var_LatLong_3;
                var_Index = var_Index + 1;
                var_P = var_P - var_Step;
            }

            var_Loop = var_Loop + 1;
        }
        var_LatLong = inclusiveSubstring(var_LatLong, 0, 15);
        var_Lat = Double.parseDouble(inclusiveSubstring(var_LatLong, 0, 6)) / 100000;
        var_Long = Double.parseDouble(inclusiveSubstring(var_LatLong, 7, 14)) / 100000;
        var_NSEW = Integer.parseInt(inclusiveSubstring(var_LatLong, 15, 15));
        if (var_NSEW == 1 || var_NSEW == 3 || var_NSEW == 5 || var_NSEW == 7) {
            var_Long = -var_Long;
        }
        if (var_NSEW == 2 || var_NSEW == 3 || var_NSEW == 6 || var_NSEW == 7) {
            var_Lat = -var_Lat;
        }
        return new double[]{var_Lat, var_Long};
    }

}
