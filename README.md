# ReverseBack-Lib

This library should contain the functionality to reverse the codes from Reverse Wherigos to actual coordinates.

## Usage

For a simple usage see: [Gitlab/SchoolGuy/](https://gitlab.com/School_Guy/reverse-back-cli).

This library is also used inside my Android-App
[Gitlab/SchoolGuy/ReverseBack](https://gitlab.com/School_Guy/reverseback).

## Tests

This library has tests which are executed on every commit and MR. This means that it should never behave incorrect.
If you find an error then please open an Issue in this Gitlab-Repository!